lec1 
=====

First we'll define some terminology.

> Hosts

End systems - typically don't bother with routing data through a network

> Communication Links

Typically the actual systems that connect things together.

Network edges
-------------

Can be subdivided clients & servers and sometimes both at the same time.

Access network: cable network
-----------------------------

Typically when have to share one line we can change the frequency of the
signal as one method to provide a distinguishment between different data
which may sometimes come from different sources.

### Home Network

Let's start with the modem. All it does it take some signla and convert
it to the proper IEEE data format(citation needed).

Typically we would then pipe that data to a router which, given a
scenario for most houses, would forward that input data to whichever
machines requested the data.

If you recall back to your discrete mathematics coursework various graph
topologies were covered and you likely noted that *star* topologies were
common for businesses since it makes it easist to send data from one
outside node on the star to another. In practice this would just mean
having the router/modem setup be one of the apendages of the star and
switch be in the middle so that the data only has to make two hops to
get anywhere in the network.

> Doesn't that mean theres one node that could bring the whole network
> down at any time?

Absolutely, which is why if you have a *very* small network with a
couple devices it's not really a problem but if you have an office full
of employees all with their own machines and wireless, printers,
servers, etc. then it's a huge problem. That's why typically a small
business or shop might be more inclined to use such a setup because: \*
It's easy to setup \* It's cheap to maintain
