# lec13

## Datagram Forwarding

Prefix matching: instead of branching on ranges with a given destination address and our address ranges, we can do a depth-first search to determine where our given address should go.
This means it will fall into one of our address ranges and we can determine which interface to forward that datagram to.

Essentiall: Instead of branching we'll do a depth first search to see where each datagram must go.
_We search the binary values._

## Output ports

This is more or less the point where packets can be dropped along its route.
The reason why is because if the queue for that router is full and we try to add more stuff in the queue from fabric.
We detect that the queue is full, and our incoming bus is full, so we drop that datat before anything else happens so that we dont mix data arbitrarily.

Those output ports will each have their own queue buffers.
However inputs can also have their own issues.

## Input ports

Just like first we read from a bus off the wire to determine where that data is meant to go.
This process takes time however so sometimes we have to drop datagrams from memory completely if our queue gets too full.


## Scheduling

Now we'll look at which packet to drop.

### Priority

Send highest priority packets first.
Say we have to decide about two packest: which goes into queue first?
In this case higher priority goes in first, then the lower.

### Weighted Fair queueing

* Generalized Round Robin

Each class gets weighted amount of service in each cycle.

## IP Datagram Format

Let's start with the header data.

Each datagram is given an ip header which tells other nodes in a network where that datagram is meant to finally end up at.
If a datagram is large however, we might fragment that datagram giving each _chunk_ their own header that tells the other routers where it's supposed to go and if that datagram is part of a larger datagram.
When the pieces finally arrive to their destinatino they  can be reassembled using the previous header information.
