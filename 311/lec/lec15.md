# lec15

## DHCP 

For a client to setup a connection with a server using DHCP we must (as a clien) send out:

```
source addres: 0.0.0.68
dest: 255.255.255.255,67
yiaddr: 0.0.0.0
transaction ID: 654
```

This is broadcasted out so that nearby serer can "hear" this request and send back some pertinent information for the client's new address on the network:

```
src: somthing
dest: ff.ff.ff.ff,44
yiaddr: something else
transactioni ID: sumting
lifetime: long time
```
The client then responds using its _previous_ information because we must first acknoledge to the server that we are going to use that new information.

## From subnet to Internet

A given machine's network address must be routed through some interface(modem) to reach the rest of the internet. 
This modem is the part between us and the internet which translates our local address to an address that is recognizable to the rest of the internet.

## Network address translation

3 things our NAT router must do:

1. Replace outgoing message addresses/port with NAT address/port

2. Remember all the ingoing and outgoing addresses and port.

3. Incoming datagrams must have their target address/ports changed to relevant local network address/ports.

Basically the NAT must translate things going in and out to the proper direction.

## ICMP

> Internet Control Message Protocol

## IPv6 

> Initially, because 32-bits is almost not enough these days.

This is becoming increasingly true as the IoT industry grows.

> No more checksums

Because we want to reduce the processing load on 

> Options

Specified in a seperate header from the top header using a flag

> ICMPv6

New stuff like _packet too big_ messages whch can be sent upstream to tell a host to reduce packet size.

The whol point of this new protocol is to primarily simplify the implementation at an atomic level, and to also reduce the strain on routers to improve quality of service.

To move towards IPv6 we can use tunneling which basically means we wrap our IPv6 datagram with an IPv4 header.
