# lec17

## Distance Vector Algorithm 

Properties of this algorithm include:

* Nodes are self updating 
	* This means that each node updates its distance table with all of its neighbors.

The main advantage of this is that we don't need a knowledge of the whole map and we can have dynamic maps that change over time.

Compared to Dijktra's algorihtm we are able to establish a much more realistic scenario, akin to how routers actually figure out how to route packets.
We're able to to do this because we don't worry about mapping out the _entire_ network which, if you sending data across countries, would be monumental in size to map out.
This is isn't even mentioning the amount of time and effort it would take to process the shortest path cost, assuming that giant network doesn't change because someone plugged in a router.
