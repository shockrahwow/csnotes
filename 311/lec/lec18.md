# lec18

## Heirarchical Routing

So far we've looked at routing from a very simple point of view.
To better model reality we'll assume a new constraint: that being the internet as a network of networks.
This means that the actual routers within each network must maintain multiple forwarding tables.
One for intra-net routing, and another for inter-net routing.

## Intra-AS Routing

### RIP
	
Popular because it was included with BSD/unix back in 82.
Uses simple distance vectoralgoritm for evaluating cost.

1. Maximum distance of 15 hops
2. Distance vectors exchanged every 30 seconds

### OSPF

> Open Shortest Path First

Uses a link state algorithm.
* LS pcket dissemination
* Topology map at each node

Security is built into the protocol itself.
We allow for multiple _same-cost paths_ 

Also uni- multi-casting 

## Inter-AS Routing

### BGP 

> Border Gateway Protocol

* Allows subnet to advertise its existent 
