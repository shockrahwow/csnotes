# lec19

## Broadcast Routing

We allow multiple identical streams to follow a _trunk_ structure wherein we only branch to a client when needed.
This keeps bandwidth usage down on a network's links.

## Multicast Problem

Take the concept from the previous section, where we have a tree structure spread across a network.

Our goal here would be to create some tree where all the _members_ for the data share the same tree.
_Also multicast groups get their own subnet class_.
This is to distinguish regular traffic from this multicast traffic[224.0.0.0].
