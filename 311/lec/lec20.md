# lec20

Link Layer(is it cool or is it whack)

_Frame_: packets on link layer i.e. layer-2 packet

MAC addresses are used at this layer more often since we don't assume that we'll ever have millions/thousands of people connecting to one network.

## Services

* Framing and link access

Yet another header is added at this point

* Reliable delivery 

* Flow control 

Receiver can report lower send rates

* Error Detection

	* Retransmission signals
	* checksums yo

## where tho

usually this is in hardware/firmware

Typically theres a microcontroller next to the some bus reading in and out whatever it can

Sender usually tacks on header

Receiver will take take analyze things then send it back up across the apic etc etc, it usually comes right back to another controller to write out onto a different bus.

## Checksum? more like check this parity 

physical world is full of noise so basic checksums won't do shit anymore.
This is why use things like parity checks.
Pretty much we take some stream, push it into a grid of n by m size
Add 1 to n & m. Where the tall side get filled with the parity of each row and the low side is filled with columns' parity.
Send this result to receiver who verifies this stuff, and we can determine single bit errors.

## Cyclic redundancy check

_even more techniques for error detection_

View a bit stream as some size\_t binary value
