# lec21

_Recall CRC_: Basically long division but with xor's instead of repeated subtractions

## Multiple Access Protocols

* point to point link

One machine on each end of the link

* broadcast 

-----
Ideallly: 
* we would make sure each node could send at whatever rate they needed to
* decentralized 
* simple to implement

## MAC protocols

Three main classes of protocols methods: 
1. channel paritioning
	* time muxing, frequency muxing etc

2. Random access
	* allow collisions and just try to recover on the fly

3. Taking turns
	* nodes take turns doing things


## Random Access 

Basically whenever there's a collision we would retransmit.

### ALOHA

Partially time slotted technique.
This means it helps us to have an accepted frame size.

Once a node is transmitting it can transmit at its full bandwidth.
There is tons of collision however, which means we have to recover a ton as well.

Pure unslotted ALOHA we basically transmit a frame as soon as is arrives which means frames can overlap.
Putting these frames into slots allows us to reduce this collision probability overall raising our efficiency.
