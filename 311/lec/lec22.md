# lec22

_This is continuation of previous lecture where MAC protocols are covered in brief._
## Taking turns

Let's now describe various methods to achieve turn taking.


1. _polling_:

	* master invites slaves to transmit data

2. _token rings_:

	* control token passed from node to node
## ARP: Address Resolution Protocol

Protocol to determine MAC address on a given device in a network.

Upon association the protocol will emplace an entry into the target device's `arp table` where we record MAC and IP address associations.

# Wireless links

Interference: everything will cause interference which means we expect interference.

Primarily for wireless connections: _Code Division Multiple Access_(asynchronus kinda)

### CDMA 

Each node receives a code which it uses to splice transmissions across the wireless link.
Each node may transmit data at an arbitrary rate since the transmissions splice into each other forming a consistent signal cumulatively.


