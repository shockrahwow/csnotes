# lec23

Wifi stuff in this lecture... jk its cellular shit wew lad


## Pieces

* base station

covers an area for users

* users

yea

* air interface

most(all) of this based on air to air connections

## Dividing up signals

Either we use _TDMA_ or _FDMA_ at the cell tower level.

## Mobility 

Since we're dealing with dealing with literally moving targets now we have to consider a few new things, like handling how cell tower's deal with people connecting and disconnecting all the time.
Moving from access point to access point we must  somehow maintain connections.

To route this kind of thing we have two options:

1. Let routing handle it

Just like previous lectures, send off some data and hope that the data gets to the right place

2. End system offloading

Let end systems deal with it via direct or indirect routing.

