# lec8 

_Continuing where lec7 left off, so keep that page handy._
Covering some of the principles of reliable datat transfer.

Typically we want a sending process to dump a message into a reliable channel so that a receiving process can process that message off that reliable channel(kind of like a pipe).
These processes would live in the _application layer_.

## Working towards Error correction

_This sub-section we'll build our way to a system of dealing with errors from nearly nothing up to soething that could reasonably work._

### V 2.2

Now instead of having `NACK` we have the recevier send two `ACK` of the previous good packet received. 
At this point our newly updated _standard_ will make the sender hang for a bit and the second `ACK` will confirm for the sender that the previous packet should be resent.

### V 3.0

Timeout: if a sender does not receive anything in some alotted time frame we should resend, replacing the double `ACK` from before.


## TODO: FIX THIS WHOLE SUBSECTION
## Errors


First we'll assume that we're not losing pacckets but only getting errors.

The receiver can try using a checksum to verify the packet was received.
This means we now have `ACK`s to acknowledge good data transfer, however for bad packets we have `NACK` saying that there was error with the checksum.

> What if the ACK/NACK is corrupted?

At this point we have to implement some kind of control sequence to show that one packet comes after another.
Each packet in a sequence can now be given a sequence number so that the receiver knows what to request if something goes wrong in the communication.

Now we can validate checksums and have sequence numbers however, if that ACK is corrupted from receiver to sender then we, _as the sender_, must resend the packet.
We only resend that packet in the sequence if the receiver requests the packet.
On the receiver side that looks like this:
* get packet who's checksum fails
* send ok for _previous_ packet

### Timeout

Say we send a packet but never get an acknowledgement of the receival we can resend after some alotted time.
```
a -> send(p0) -> b
a <- ack(p0) <- b
a -> send(p1) -> b
.
.
.
a -> resend(p1) -> b
```

### Performance 

Up until now we have checksums, timeouts, ACK's and the ability to resend. 
What does the performance look like at this point?
```
U_sender = (L/R)/(RTT+(L/R))
```

L=length of packet
R=link speed
RTT=Round-Trip Time


