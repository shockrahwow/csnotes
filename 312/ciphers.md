Active v Passive Attacks
========================

Base Definitions
----------------

Passive: compromising a system but not necessarily doing anything apart
from *watching*

Active: compromising a system while doing something to the system apart
from infiltrating it

Loosely speaking
----------------

*Passive* can be just like listening in on a conversation(eavesdropping)
where *active* is like jumping into the conversation and trying to do
something to it.

When/How would either happen?
-----------------------------

If the result of an attack is to actually trigger some code to run then
usually we need to first gather the information required to understand
how to make that happen. The reasoning is straightforward: if you don't
know how some system works then it's much harder to exploit that system.

Random example: Using a keylogger to log keystroke before sending those
logs to a server for processing could be a passive attack since you're
still in a *gathering data* sort of mode. Finally using that data to
trying logging into some service would be the active portion of a
full-scale attack.
