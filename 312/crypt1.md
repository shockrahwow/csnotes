# Cryptography - First steps


## Theoretical mumbjo jumbo

I'm going propose right now that there are two principles to _all_ cryptography: 

1. Key production
2. Algorithmic reliability

In plain English: the two main problems we encounter when trying to safely[1] encrypt a set of data, or more generally/simply, _a thing_ is producing some kind of key that can't be reliably guess + an algorithm which can doesn't subject itself to being breakable[2].

## Ciphers

_All_ ciphers work on __one__ principle: replacement of characters/things in the thing we are trying to cipher.

Some popular forms of this

> What is Rotational ciphering

Let's take `abc` and increase each character by `x=2`.
We end up with `cde`.


> I'm a malware developer how do I cipher binary data?

Easy, your alphabet isn't a-z or A-Z but now `0x00 - 0xff` or, if you want to use pairs of bytes your alphabet is `0x0000 - 0xffff`.
Again apply the same logic from above to the new "_alphabet_".


> What are Substitution Ciphers

Let's shuffle our normal everyday alphabet from `abcde...` to `uwfcj...`.
Now we take these two :

```
abcde...
uwfcj...
```

We can now say that all the `a`'s in our _thing_ that we want to cipher gets replaced by `u`'s, `b` by `w` and so forth to the end.

> That's perfect let's always use that!

Except you can use some statistical magic[3] to analyze the frequency of the ciphered letters and make a better guess at what the message might be.
Also with a small character set there would be less to fuddle through, also keep in mind that cpu's operate in orders of _billions of operations a second_ which means you character set needs to be comically large to ensure a processor doesn't reverse the cipher in seconds/minutes.

## Encryption

> hold up isn't cipher a form of encryption? 

Yes&no. No because ciphers operate in-place as they are all just replacing things with other things, usually of the same length/shape/whatever.

> Where is the distinction?

We now take our _key_ and _data/thing_ and produce a new seemingly unrelated _encrypted thing_.


So `abc` thrown through some encryption might turn into `234123j4h1` for whatever algorithmic reason(mileage may vary depending on algorithm).


> What is Symmetric & asymmetric key encryption?

Conceptual answers:

Symmetric: One key to encrypt/decrypt or _lock/unlock_ the _thing_, just a like a typical lock on a door.

Asymmetric: One key can encrypt/lock the _thing_(public) and one can open it(private key).
The keynote here however is that the private key should be _really hard to guess_.

## Footnotes

1. 

2. 
