# Cryptographic Hash Functions

## Random Jargon

_If you made it this far you know what all of this is but we need these terms because "muh academic vernacular"_

* Variable Length Input
* Fixed size output

```python
a = {
	'first',
	'second',
}
```
Both `first` & `second` as inputs result in distinct references of the same size.

* One-way property

You should **not** be able to use a hash result(i.e. reference) to find an input. 

In English: _Only inputs can find outputs, not the other way around._

* Two-way property/ Collision-free Property

**2** identical data inputs will map to the same output

## What does a hash provide?

* Integrity checks

* Message Digest / Authentication

_Hashes are not secure ffs_. When we say authentication we really mean that we are checking to ensure there has been no changes to the original message.
_NOTE:_ this really doesn't guarantee anything in communications susceptible to man-in-the-middle attacks.

**Let's get real for minute:** Think of it this way a message is like a car and the key is our hash. If the key doesn't fit in the slot then its not our car, **but** if we sent our car + key oversees whoever steals the car + key has full control and we lose security effectively. Using SSL is _kinda_ like putting that car + key combo in a box that nobody except the recipient can open.

