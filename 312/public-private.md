# Asymmetric Key Encryption(Public/Private)


Think of a box that we put things inside of:(put simply) 

* Private key: can open the box
* Public key: can lock the box

The idea works on the principle that public keys are public so anyone can lock a message down but only the owner/creator of that public key can open those locked messages with their private key.

## Public Keys

Can be used to open something if it was locked with a private key.

## Private Keys

If used to lock something the public key can be used to then open the box.
_The catch_: that message is also signed so we know exactly who the message is coming from.

## Both together

> Message => Lock(message, private key)

_Sign_ the message

> Signed Message => Lock(signed message, public key)

Lock the message like normal

Once the intended person has the package they:

* Open it with their private key
* Check the signature 
* Find the public key for that signature
* Open the remaining layer with the public key

That last part only works because locking with a private key allows the public key to open the box afterwards.

