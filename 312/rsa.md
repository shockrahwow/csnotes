# Procedure

We have 4 primary values that we deal with:

* p
* q
* e
* m

There are a few components which must be calculated before we can safely determine a cipher text:

`n = p * q` : note that `p` and `q` values should be primes in this case.

`Φ(n) = (p - 1) * (q - 1)` is used later to verify that we have a value `d` which is the inverse of `e`. _We call this the quotient function_.



## Encryption

To produce a cipher text `C` we take `m` and raise it to the power of `e`(from earlier) then take the modulo of it by `n`:

```
C = (m^e) % n
```

`m` is the desired message to encrypt.

The public and private keys are using the above cipher text functions whose unknown parameters are passed as follows

`PublicKey(e, n)` 

`PrivateKey(d, n)` 

## Decryption

The reverse of this is the following:

```
M = (c^d) % n
```

## E inverse e^1

To find `d` the following _must_ be true: `GCD(e, Φ(n)) == 1`. 
If this is not the case then there is no `d` or `e^-1`.


> how do i actually this trash tho???

Let's say we have `e=17` and `Φ(n)=60`:

We know the GCD(17,60) == 1 [17 is prime] so we can find an `e` inverse.
_Check the notes at the bottom for an easy to rationalize method of verifying this_.
