# First half of course

## Attack Types

* Known Plain text
* Known Cipher text

## Ciphers

_Note that we focus(a ton) on the properties of these ciphers both forwards & backwards_

* Caesar Cipher
* _Rot Ciphers_
* Rail Fence
* Row Transposition
* Block Cipher
* Feistal Cipher

## Encryptions

* Triple DES
* DES
* AES
* Public/Private Key Encryption

