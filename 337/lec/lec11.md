# lec11

_diagrams references implied for now_

Sequential Logic: at this point we effectively are dealing with state(_state machines_). Simply put we have _memory_ now.

## State Tables

In Q~s~ is our _Current state_ while Q~s+1~ is the next state

![](../img/lec11fig1.png)

| A | Q~s~ | Q~s+1~ | 
|---|---|---|
| 0 | 0 |  0|
| 0 | 1 |  0|
| 1 | 0 |  0|
| 1 | 1 |  1|


We can try the same thing with an `or` gate:

![](../img/lec11fig2.png)

Keeping in mind that our effective input here is only `A`.

## Latches

Namely we are going to look at set-reset latches.

They should be able to do two things:

* store a state
* change state upon appropriately changed signals.

![](../img/lec11fig3.png)

Note that the above state machine the two rows show up as illogical; because both don't make sense in that context.

The actualy gate implementation of the above would look like the above.

![](../img/lec11fig4.png)

The same can also be done with `nor` gates making the whole operation much more efficient on transistor usage.
The figure below is a more common implementation of a _Set-Reset Latch_.

![](../img/lec11fig5.png)

> Interesting but what is it used for?

Setting a value or reseting a value to 0.
That's all there is to it; either want to _set_ our ouput, or we want to reset it to zero.
This is the first step in creating a device which can _actually_ store information to be used later on, in other words, memory!

First we'll clean up our input: we are allowed to set _and_ reset which conceptually doesn't really make any sense since you should only be able to do one at a time.
To stop this input from even being accepted we'll used _one_ input which splits into both `nor` gates [D].

![](../img/lec11parsedinputlatch.png)

Next we want to decide _when_ to store the data because then we would even more control over our future _memory_.
To do this we'll need some signal to dictate when we should pass our desdired data to our output(_which could be to a memory bank_).
Our inputs now have `D` for the data we have now, and newly `C` for control(ling) if we want to store our data or not[1=yes 0=no].

![](../img/lec11controlledlatch.png)

At this point this is what we call a _D-latch_
### D Latches

We saw earlier that we can now store data based on some control.
Let's imagine that this control will regularly pulse between 0 and 1... similar to a _clock_.
This means that if D wants to spas out of control we don't really care because we're going to allow `D`'s value through our controlled latch __only__ when `C` is high.

This is all a _D-latch_ really is. 
It is just a mechanism to "read" a signal into some output signal _whenever we want to_.
Even though the signals will keep flowing the output will be under our control, it just so happens that we are using a clock to control it out of convinience.

## Clocking & Frequency

The period of the square wave in this case can be used to find the frequency.
We simple note that `1/T = F`.
This frequency is measured in cycles/second or _hertz_.

![](../img/lec11squareWave.png)

### Setup time & Hold time

Setup time would be some aount of time after the previous point where we wait for the combinational logic to perpetuate its results into memory.
A short period of time in the valley would be setup time

Hold time is the time that we wait before we start feeding input into our combinational logic(unit). 
Say we wanted to start our combinational logic at the beginning of one of our plateaus.


### Flip-Flop & Edge Triggering

Referring back to the square wave let's say we want to grab the value of `D` when the control signal rises high, but _only_ when it rises.
To do this we'll use the folowing structure:

![](../img/lec11flipflop.png)

Grabbing the value of `D` when the clock signal falls low is just as easy however the answer will not be shown here.
I suggest trying to find the answer yourself then looking up what such a logic diagram would look like.

For pedantic sake here are the simplified diagrams of the above.
_Only because they are so common._

![](../img/lec11flipflopsimp.png)

