# lec15

If we want to write our functions in MIPS then we need some way of not just returning from a _sub-routine_ but also pushing arguments to each function call.
