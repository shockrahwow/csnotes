# floating point

mips uses ieee754 

## parts of floats

1. Sign bit
2. Exponent
3. Mantissa

_32-bit floats btw_

## Sign 

bruh

## exponent 

This is coming from scientific notation so we have `2.0E3`

This means our 8 bit exponent is `0000 0011` + `0111 1111`

## Mantissa 

let's roll with `2.0E3` 

Mantiss of 2.0 = 0 <- 23 times

We have 23 `0`'s because the decimal portion of our number `2.0E3` is 0.
