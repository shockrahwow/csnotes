# lec3

## One's & Two's Complement

_Previous lecture went over signedness of numbers so this section won't as much_.

First we'll deal with flipping bits: this is where you may hear the term _1's complement_.
While not very useful on it's own for most purposes it does help get closer to creating a seperation between _positive_ and _negative_ numbers.

The only other step after flipping all the bits is just adding 1.

`1001 1110` becomes `0110 0010`.

> shouldn't that last 2 bits be 01?

Close, the reason why we have `b10` is because if we: `b01 + b1` the `1` will carry over to the next bit.
The actual term for this is just __negate__; the other way around is essentially cannon fodder.

>Ok, but what does that look like _assembly_ the thing I came here to learn.

Most assemblers accept something like `neg targetValue` however you can also use an _exclusive or_[`xor targetValue, 0xFF`]. Keep in mind that the immediate value should be sign-extended to reflect the proper targetValue size.

Two's complement proccess:
1. Flips bits
2. Add 1

## Sign Flag 

Set whenever we produce (any) number where the leading bit is set(1).
Regardless if we're dealing with signed or unsigned data.

If we mess with some data but the sign bit remains the same then our sign flag just stays in its current value.


