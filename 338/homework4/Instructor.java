/*
Class:
	Instructor
Description:
	Structure to represent a instructor in the school system
	
*/
public class Instructor {
	public int id;
	public String name;
	public String email;
	public String phone;
	public Instructor(int id , String name, String email, String phone) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.phone = phone;
	}
	public String toString() {
		return this.name;
	}
}
