import static org.junit.jupiter.api.Assertion.assertFalse;
import static org.junit.jupiter.api.Assertion.assertSame;
import static org.junit.jupiter.api.Assertion.assertNull;
import static org.junit.jupiter.api.Assertion.assertNotNull;
import static org.junit.jupiter.api.Assertion.assertEquals;
import org.junit.jupiter.api.Test;

class Tester {
	public static School tester;

	Tester() {
		testSchool = new School("Test School");
	}

	@Test
	public void addInstructor() {
		assertEquals(false, tester.addInstructor(123, "name", "asdf@mail.org", "1123-12-312"));
	}

	@Test 
	public void addCourse() {
		assertEquals(true, tester.addInstructor(999, "School of what", 100, "New build 7"));
	}

	@Test
	public void getCourse() {
		assertNull(test.getCourse(205));
	}

	@Test 
	public void deleteCourse() {
		int len = tester.courseList.size();
		tester.deleteCourse(205);
		int newLen = test.courseList.size();
		assertSame(len, newLen);
	}


	@Test
	public void addStudent() {
		// because the course doesn't exist this should return false
		assertSame(false, test.addStudent(456, "Billy", 717, 89.0f, "B"));
	}

	@Test
	public void getStudentInfo() {
		assertNotNull(test.getStudentInfo(8888));
	}

	@Test
	public void graduateStudent() {
		// no student should have this id, ergo this should fail
		assertFalse(tester.graduateStudent(9090));
	}

}
