public class Course {
	public int id;
	public String title;
	public int instructorID;
	public String location;
	public int studentCount;

	public Course (int id, String title, int instructorID, String location) {
		this.id = id;
		this.title = title;
		this.instructorID = instructorID;
		this.location = location;

		this.studentCount = 0;
	}

	// updates the room location of a given course ex. BIT-104  to BIT-110
	public void updateLocation(String newLocation) {
		this.location = newLocation;
	}

	public String toString() {
		// Instructions were not clear on what the toString should return so this is my interpretation
		return id + " - " + title + " " + instructorID;
	}
}
