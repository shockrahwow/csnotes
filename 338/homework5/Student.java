import java.util.HashMap;
import java.util.ArrayList;
public class Student {
	public int id;
	public String name;
	// courseID, grade for that course
	public HashMap<Integer, Grade> grades;

	public String status;

	public Student(int id, String name, int courseId, float floatGrade, String letterGrade) {
		this.name = name;
		this.id = id;
		grades = new HashMap<Integer, Grade>();
		grades.put(courseId, new Grade(floatGrade, letterGrade));
		// default student status
		this.status = "Enrolled";
	}
	public String toString() {
		return "Student  " + this.id +
			" Name: " + this.name +
			" Status: " + this.status;
	}
	public float averageGrade() {
		float sum = 0;
		int count = 0;
		for(Grade g : this.grades.values()) {
			sum += g.num_grade;
			count++;
		}
		return sum/(float)count;
	}
	public void graduate() {
		for(Integer i : this.grades.keySet()) {
			// remove from all the courses
			this.grades.remove(i);
		}
		this.status = "Graduated";
	}
	public void enroll(int courseId, float num_grade, String l_grade) {
		grades.put(courseId, new Grade(num_grade, l_grade));
	}
}
