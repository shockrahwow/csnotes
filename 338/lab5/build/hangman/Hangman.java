package hangman;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class Hangman extends Application {

    Image[] images = new Image[7];
    ArrayList<String> words;
    Game g;
    ImageView imageView;
    Text text1;
    Text text2;
    TextField textField;

    @Override
    public void start(Stage stage) throws Exception {
        
        words = new ArrayList<String>();
        readFile();

        g = new Game(pickRandomWord(), 6);
        
        try {
            //load image files 
            images[0] = new Image(new FileInputStream("./h1.gif"));
            images[1] = new Image(new FileInputStream("./h2.gif"));
            images[2] = new Image(new FileInputStream("./h3.gif"));
            images[3] = new Image(new FileInputStream("./h4.gif"));
            images[4] = new Image(new FileInputStream("./h5.gif"));
            images[5] = new Image(new FileInputStream("./h6.gif"));
            images[6] = new Image(new FileInputStream("./h7.gif"));
        } catch (Exception e) {
            System.out.println("Error. " + e.getMessage());
            System.exit(0);
        }

        imageView = new ImageView(images[0]);
        text1 = new Text("Guess a letter or ask for hint.");
        text2 = new Text(g.getDisplay());
        textField = new TextField();
        textField.setOnAction(new GameController());
        VBox vbox = new VBox(10);
        vbox.getChildren().addAll(imageView, text1, text2, textField);

        //Creating a scene object
        Scene scene = new Scene(vbox, 250, 350);
        stage.setTitle("Play Hangman");
        stage.setScene(scene);
        stage.show();
    }

    public class GameController implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent ae) {
            String user_input = textField.getText();
            //DEBUG System.out.println(user_input);
            if (user_input.length() == 0) {
                text1.setText("Enter a single letter or enter hint.");
                text2.setText(g.getDisplay());
                textField.setText("");
            } else if (user_input.equalsIgnoreCase("hint")) {
                int rc = g.doHint();
                imageView.setImage(images[6 - g.getRemainingGuesses()]);
                if (rc == Game.WON) {
                    text1.setText("You won!");
                    text2.setText(g.getDisplay());
                    textField.setText("");
                } else if (rc == Game.LOST) {
                    text1.setText("");
                    text2.setText("Game over.  The word was: " + g.getWord());
                    textField.setText("");
                } else {
                    text1.setText("Enter a guess or hint.");
                    text2.setText(g.getDisplay());
                    textField.setText("");
                }

            } else {
                char c = user_input.charAt(0);
                int rc = g.processGuess(c);
                switch (rc) {
                    case Game.BAD:
                        text1.setText("No " + c + " in the word. " + g.getRemainingGuesses() + " attempts left.");
                        textField.setText("");
                        imageView.setImage(images[6 - g.getRemainingGuesses()]);
                        break;
                    case Game.GOOD:
                        text1.setText("Yes. There is a " + c + " in the word.");
                        text2.setText(g.getDisplay());
                        textField.setText("");
                        break;
                    case Game.LOST:
                        text1.setText("That was your last guess. Game Over");
                        text2.setText("Word was: " + g.getWord());
                        imageView.setImage(images[6]);
                        textField.setText("");
                        break;
                    case Game.WON:
                        text1.setText("You won!");
                        text2.setText(g.getDisplay());
                        textField.setText("");
                        break;
                    case Game.REPEAT_GOOD_GUESS:
                        text1.setText("You already guessed that letter.");
                        text2.setText(g.getDisplay());
                        textField.setText("");
                    case Game.REPEAT_BAD_GUESS:
                        text1.setText("You already guessed that letter.");
                        text2.setText(g.getDisplay());
                        textField.setText("");
                        imageView.setImage(images[6 - g.getRemainingGuesses()]);
                }
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
    
    public void readFile(){
        try {
            File f = new File("words.txt");
            Scanner infile = new Scanner(f);
            while (infile.hasNext()){
                words.add(infile.nextLine().trim());
            }
            infile.close();
            
        }catch (Exception e){
            System.out.println("Error exception. "+e.getMessage());
            System.exit(0);
        } 
    }
    
    public String pickRandomWord() {
        int k = new Random().nextInt(words.size());
        return words.get(k);
    }

}
