import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/* */
public class temp extends Application {
	// start application things
	@Override
	public void start(Stage stage) {
		// label for entry text box
		Label input_label = new Label("Farenheit: ");
		
		// set up the X window
		Scene scene = new Scene(new StackPane(input_label), 640, 480);
		stage.setScene(scene);
		stage.show();
	}
	public static void main(String[] args) {
		launch();
	}
}
