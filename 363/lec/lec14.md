# lec14

Let's say we have a massive dense index, so large that we can't fit it into memory.
We can use this dense index to create a sparse index off of that target dense index.
Basically we're indexing the index to reach some data on disk that is very big.

We can of course take this even further to index and index which is indexing an index of a table.
This concept leads us to B+ Trees.

## B+ Trees

This type of tree here is a self-balancing tree.
This means that as we add rows our indexes the structure will adjust as necessary, meaning the indexes are updated and the pointers in the indexes are updated so that the height of the tree remains balanced.

The leaves on the tree will _always_ have pointers to the target data which we've built a tree upon.

### Overhead

> found on disk or ram?

> whats the cost for balancing the tree everytime

## Hashing

### Direct Indexing

We can create some index where each entry is an index number with a pointer to that respective entry.
If we have id numbers that aren't right next to each other then we have a problem: we also have to include every intermediate values in our table.

### Hash Function

Now we'll have some function that takes an input[key] and generates a value[index].
_For now we'll assume we are dealing with collisions with chaining._


