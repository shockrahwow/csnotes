# lec16

Let's now go into how we build a utility like the _transaction_ in SQLite.

## Problem Statement

> Why we need or care about transactions.

If we have tons of users all trying to access a databse to say, reserve a hotel room, we need to make sure that each operation doesn't fail or write over each other.
Otherwise we're going to have tons of undefined behavior.

## A.C.I.D Principles 

### Atomicity

Mneumonically: _all or nothing_

Either everything in our transaction happens, or none of it happens.
The reason why we care about this is because we want to be able to _recover_ from problems, like a power outage for instance, or some error which causes a halt.

To acheive this we have to log everything we're going to do.
Before we do anything in our transactions, we log what we're going to do, what changes are being made and what those changes are.

WAL: _write-ahead logging_

### Consistency

Like the name implies we mean to say that our transactions should result in a predictable output everytime.

### Isolation

Transactions should never be able to peek into another transaction.
As the name implies the transaction runs alone.

### Dependability

Essentially once we reach the end of a transaction we should commit those changes to the database.
This way if something goes wrong, where the whole database needs to be shutdown, our changes should still be there.
_Basically this means that we dump anything our transaction buffer onto disk_.

To achieve this we must verify that the changes were actually committed to the disk.


## Serializability

What we ultimately  want is to be able to operate on multiple transactions while also being able to get the same result as if we had done everything in linear order.
We want that result because it maintains isolation for each transaction.

## Transaction Schedule

If we have two complex transactions that need to run then we can schedule them in some manner.
Sometimes it means that we do one transaction first then another, and sometimes it means we do pieces of both in some order.
The latter is known as _interleaving_.

Just like individual transactions we can serialize schedules.

### More on interleaving

We mentioned interleaving earlier.
Basically this just means that we run part of one transaction then another part of a _different_ transaction.
We only do this if the result of this operation is the same as running them in a serialized fashion.
