# lec17

The previous lecture we covered methods and principles of transactions.
This time around we'll take care of proper ordering of operations.

## Operation Order

If two\* transactions work on two different data items then we know that they shouldn't collide in their operative results, therefore the order wouldn't matter.
The order matters if there is a collision between transactions on similar data.

_Conflict Serializability_ : the ability to swap an interleaved schuedule into a serialized schedule while maintaining the conflict result from the start to the end.

## Determining Serializability

We can go through a schedule where each transaction is placed into a graph as a node.
We draw edges from each node to another if say we run into a read in transaction-A, followed later on by a opposing write action in another transaction.
The opposite also applies.

Our schedule is not serializable if we have a cycle in the resulting graph.

## Locks

Exclusive lock: process locks the database for itself. 

Shared lock: allows others to put locks on the databse but not exclusive locks

There are some drawbacks to using locks, especially if done poorly.
If transaction-a locks some data, say exculsively, but doesn't release the lock before another transaction tries to use that data means that we may end up in a state where everyone is locked out of certain data.
For this reason we use a special locking protocol to take care of this exact scenario.

The state where everyone is locked out of something is called a deadlock.

### Two-Phase locking Protocol

The two phases include the _growing_ and _shrinking_ phase.
This means we are getting more and more locks before we finally release locks until there are none left.
We don't mix locks and unlocks however, so `[lock lock free lock free free]` isn't valid but `[lock lock lock free free free]` is fine.
We get two main advantages from this:

1. Serializability is maintained
2. Dead locks are easy to find

Keep in mind however, deadlocks still happen with this protocol.




