# lec18

Using graphs & trees to avoid deadlocks.

## Trees

If we have a tree filled some data that we want to access. 
With our first accessing into the tree we may lock whichever node we want, however, every subsequent lock after that point must happen _only_ if the parent node to that target is locked.

The main disadvantage to this methodology is that if we want to access the root node and a leaf node, it means we must do a lot of intermediary locking.

## Snapshot Isolation

For this strategy we're going to scrap the idea that we're going to be using locks, graphs or even trees.

Instead, when a transaction is about to run, we take a snapshot of everything we're going to modify, then work from there.
When we commit on the first transaction we'll query to see if anything else has changed the data we're trying to write to.
If nothing comes up we commit with no issue.
If something does come up we abort and restart the transaction with a new snapshot, _this time with the new stuff_.
This time around we should be ok to commit.

The overhead comes in hard if we have to be correcting transaction but, if we don't find ourselves doing that too much then it beats graphs and trees since there's barely anything to maintain.
