# lec19

Let's say you are asked to model a business's data.
The first question you may have is, _where do I even begin_.
If you are in charge of this kind of project there are some things to consider: 

* Scalability for future changes
* What kind of data you are dealing with

## Design process

1. Understand the problem at hand and needs of users
2. Create conceptual design

## Entity-Relationship (ER) Models

Used for conceptual design.
There are 3 building blocks for an ER model:

* Entities
* Relationships
* Attributes

For the rest of this lecture we'll be using a book store as our working example to get through this concept.

## Entity & Entity Set

Often with entities we're really referring to real world _things_ which have properties of their own.
An entity for a bookstore would just be something like a book, author, or a publisher.
We would have to consider these things when modeling the pertinent data in regards to the business or organization, because we have to keep track of these things to ensure that the business runs smoothly.

If we keep track of things like the books in our store, then we might avoid accidently ordering too many of the same book, or running out of one specifically.

## Relationships

A _relationship_ is simply an association between entities.
Furthermore entities, can participate in relationships by simply being related to some other entity.

Coming back to our exmample, books and publishers, are two entities whom participate in a relationship together.
Likewise we can two books which may be a part of a long running series, which means they should be related.
This would mean we have a book in a relationship with a book.
Each book however, takes on a different _role_ in the relationship; perhaps one book is the sequel to the other.

A more clear example might be that a book has an author which means the two must be related, therefore we may create associate some kind of relationship.
Likewise the same book entity may participate in another relationship if appropriate, like with the publisher.

## Mapping Cardinalities

> One-to-One Mapping

Say we have to entity sets, where each entity in each set is related to one entity in the opposing set[_ex. every office has 1 instructor_].
A is in a relationship with B or, B is in a relationship  wth A.
Both explanations are fine and valid.

> One-to-Many/Many-to-One

All the entities in one set are related to by _at most_ one entity from the other set[A student can have at most one advisor].
The student set would by one-to-many while the advisor set would be many-to-one.

> Many-to-Many 

Each entity in a given set may have zero or more relationships with entities in an opposing entity set.
Likewise the inverse is also true.
