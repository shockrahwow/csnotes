# lec22

## Functional Dependancy

If we have an attribute `a` that could produce `b,c,d` reliably everytime then we would only need to keep track of `a` instead of keeping track of all the repeats because the dependants depend on `a`.

Example:
```
building -> roomNumber
```
This one makes sense since we would likely be able to find duplicate room numbers in different buildings.
Most places have a name for each building however.

## BCNF

> Boyce Codd Normal Form

If we have a key which sources our redundancy we're ok because we can easily find all redundancies.
If we're not looking at a key then we could be in the middle of a dependancy nest.

Say we have a schema like `advisees(inst_id, student_id, student_name)`.
Student name depends on `student_id` so there's no reason to have it in the schema.


A schema follows BCNF if there's a non-trivial functional dependancy

* x -> y for r
* then x is a superkey for r

## Example

```
instructor_offices(inst_id, name, office)
```
