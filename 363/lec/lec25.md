# lec25

## Master-Slave

Here we'll have a master node which handles updating data, but the slaves only deal with reads.

## Peer to Peer 

In this case we still have a reverse sink where things are input and then sent through some pipes to all the other nodes on that network.

## Redundancy

Apart from being redudant with data we can also do the same thing with workloads.

## CAP Theorem

* consistency 
* availability 
* partition tolerance

Definition: You can't have all of these without some large tradeoffs.(shit happens all the time)

If you optimize for accessability then you may not be able to optimize for consistency without sacrificing for one or the other.
Say we have the following constraints:

* n = number of replicas
* w = number of nodes that must ack a write
* r = number of nodes that must ack a read

If you optimize for reads: r=1 w=n 

Then reading is quick, meaning your data is more accessible, but it's also less reliable. 

## Pessimistic Replication

Traditional strategy: 

* block access to data until it is up to date


