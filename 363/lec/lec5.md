# lec5

## Lab 

This lecture will have a lab activity in `cst366/lab/1994-census-summary.sql` with instructions found in `single-table-queries-2-lab.pdf`.


## Distinct Values

* Mininum - min(field)

Finds the smallest value in the given filed

* Maximum - max(field)

Find the largest value in the given field

Say we have a column where we know there are duplicate values but we want to konw what the distinct values in the column may be.
SQLite3 has a function for that: `select distinct field, ... from table;` 

* select substr(field, startIndex, length) ...

_Note_: the start index starts counting at `1` so keep in mind we are offset `+1` compared to other language like C.

## Joins

Now we want to join to tables together to associate their respective data.
To accomplish this we can perform a simple `join` to combine tables. 
Important to note that a simple join does not necessarily take care of duplicate fields.
If we have duplicate fields we must denote them as `target.field`. 
Here `target` is the table with the desired table and `field` is the desired field.

## Type Casting

If we have say `"56"` we can use a cast to turn it into an integer.

> cast(targetString as integer)

This will return with an error if a non number character is given as input to the cast function, here in this example we denote it with `targetString`.


