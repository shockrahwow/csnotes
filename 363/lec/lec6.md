# lec6

## Lab activity

This lecture features a lab activity in the lab/ directory named: `courses-ddl.sql` with instructions in `simple-joins-lab.pdf`.

* Note: Just make sure to read in courses-ddl.sql _first_ then courses-small.sql _second_ otherwise there will be random errors.(I'm not taking responsibility for that garbage so don't flame me)

## Natural Joins

`Natural Joins`: allows us to join tables while getting rid of duplicate columns automatically.

Form:

```
select columns_[...] from tableLeft natural join tableRight
```
While there is no need to write extra `where` statements there is also the issue where there may be accidental matches since attributes are dropped.
This implies that if two tables have attributes with the same field name, then only one will be returned in the resulting table.
