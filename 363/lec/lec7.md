# lec7

## Lab Activity

This lecture has two correspondnig lab activities in `lab/` using `1994-census-summary.sql` with instrucctions on `aggregation-lab.pdf` and `nested-subqueries-lab.pdf`.

## Null Operations

Take the following table as a trivial example of working data

| a | b |
|---|---|
| 1 | 2 |
| 3 | N |

Where `a` and `b` are attributes and N signifiies a NULL value.
If we `select a+b from table` we only get back 2 rows like normal but the second row is left empty since we are operating with a NULL value.
Even if we use multiplication or some kind of comparison against null we simply ignore that row since NULL in sqlite3 doesn't mean 0.
Instead NULL in sqlite3 actually represents something that doesn't exist.

> count will treat NULL as 0 however

This is the only exception to the _ignore NULL_ "rule".

## Aggregation

This section we'll deal with functions similar to `count average min max`.
We call these functions _aggreagate_ functions because they aggregate multiple data points into one.

> round(integer)

Rounds off the floating point number to some level of precision.

> group by _attr_

This will group attributes to gether in the result of a query

> having(attribute)

Similar to `where` however we only care about group scope in this case.

## Nested Subqueries

Recall that when we perform a query the result is a table. 
We can leverage this and perform some query to query a resultant table to further our ability to filter results from a table.
