# lec8

## Lab

The lab exercises for this lecture can found under `lab/` as `db-mods-transactions-lab.pdf`.


DB Modifications, plus transactions

## Modifyinig Data

Since we're dealing with data we may need to add, delete or modify entries in some table.

When we have inserted data before we have done simple insertions like in the previous lab exercises `insert into tableName values(...);`.
Where the arguments are listed in the same order as they are listed in the table structure.

However, we can pass arguments by name, elminating the need to provide them in a rigid order: 
```
insert into tableName(list, of, attributes) values('respective', 'data', 'entries');
```

We can also move things from one table into another table.
```
insert into targetTable select ... from hostTable;
```

### Deleting

```
delete from tableName where ...;
```

Deletes a _whole row_.
Caution: the delete operation also accepts tables as valid arguments so a query that returns multiple rows as a table will be deleted in the `targetTable` mentioned earlier.

### Updating entries

```
update tableName set attribute=123 where def='abc';
```

The above updates an attribute based on the condiftion `where def='abc'`.

## Transactions

Set of instructions which upon failure do not modify any state.

```
begin;
	// set of commands
	// wew
end;
```

## Inner/Outer Joins

> left (left outer)

_the outer part is implied so it's unnecessary to write it in_

