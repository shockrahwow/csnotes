class Node():
    def __init__(self, c, weight):
        self.c = c
        self.freq = weight
        self.left = None
        self.right = None
  
    def __repr__(self):
        return f'{self.c}|{self.freq}'

class Huffman:
    def __init__(self, string):
        self.heap = []
        self.charMap = self.charFrequencies(string)

    def charFrequencies(self, string):
        ret = []
        for i in string: 
            if i in ret:
                ret[i] += 1
            else: 
                ret[i] = 1
        return ret.sort()

if __name__ == '__main__':
    text = input()
    binary = input()

    # encode the first given string to build a tree from it 
    huff = Huffman(text)
    print(huff.charMap)