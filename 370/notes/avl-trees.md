# Self balancing trees aka AVL trees

_time complexity will be in terms of height_

> Height:
	distance from nulls after leaves
	* impl: _root will typically have the largest height value_

> Balance:
	left_height - right_height
	we can discard the || if we want negative balance vals but it really shouldn't matter
	basically: we want the balance for each node to be 1 or 0.

# Self balancing part

We maintain balance through the tree's lifetime as we insert/delete things into the tree

> Insertion/Deletion
	* just like BST but we balance starting from the new node

# Restoring balance

There are 4 special cases when dealing with restoring balance:

	1. left-left
	2. left-right
	3. right-left
	4. right-right

# Tries


