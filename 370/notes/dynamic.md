# """"Dynamic Programming""""


> Take a large problem
> Break it up
> Solve the pieces



# Definitions

_Memoization [Recursive dynamic programming]_
: 	top-down approach
:	Go from big problems and build downward

_Tabulation_
:	Bottom up approach
:	Go from little problems and build up


# Recursive compiler tricks

Normal iterative: 
```
start:
...
jcond start
```

Normal recursion(under non-priveleged procs):

Each "iteration" creates a new frame of whatever size:
hence we approach the hard stack limit

_Sick guess_:
```
	At compile we should be able to predict an incoming iteration from a 
	recusive endpoint.

	? is it just parameterized jumps ?
```

_Answer:_ 
```
	Tail recusion optimization

	Takes the last statement of function 
	Assuming its a recursive call, we can replace our current stack frame's arguments
	The only state that changes between calls then is just the arguments
```
In this sense we are turning a recusive functino into an iterative one because the whole state is held in one frame & we only ever jump within our routines address space.

However, this tail optimization can only happen at the tail of a routine.
