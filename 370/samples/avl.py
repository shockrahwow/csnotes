class Node:
    def __init__(self, key, value, left=None, right=None, height=0):
        self.key    = key
        self.value  = value
        self.left   = left
        self.right  = right
        self.height = height

class AVLTreeMap:
    def __init__(self, node):
        self.root = root

    # Data related operations
    def put(self, key, value):
        # go until we find a good place to put our dank value
        curr = self.root
        while True:
            # going left maybe
            if key < curr.key:
                # see if we can stop now
                if curr.left is None:
                    curr.left = Node(key, value)
                    break
                if curr.right is None:
                    curr.right = Node(key, value)
                    break

    def get(self, key):
        curr = self.root

        if curr is None:
            return None

        # check if we found the thing
        if key == curr.key:
            return curr

        if key < curr.key:
            get(curr.left)
        else:
            get(curr.right)

    def remove(self,key):
        pass

    def levelOrder(self):
        # Return case
        if node == None:
            return

        # Go left
        self.levelOrder(node.left)

        # Visit
        print(f'{node.value} ', end='')

        # Go right
        self.levelOrder(node.right)


    # tree related operations
    def rotateLeft(self, node):
        pass
    def rotateRight(self,node):
        pass
    # returns the height of a _node_ not the whole tree
    def getHeight(self, node):
        pass

    # returns node balance
    def getBalance(self, node):
        _left = 0 if node.left is None else node.left.height
        _right = 0 if node.right is None else node.right.height
        return _left - right
