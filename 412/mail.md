# Email Service

For any email service we have some goals that we would like to keep in mind as we either choose what service to use or build one ourselves:


> Standards 

Try to keep things with standard protocols

Especially if you try to make your own mail service we don't try to make our own protocols because that makes the service highly incompatible with basically everything else.

> Automation

Have some system that can create emails for you instead of manually creating them.
We don't want to go to some database and manually type out all the commands to create a new email everytime; even a set of scripts might be annoying to use unless once every so often.

An example scenario is a mass hiring season where each new employee requires their own company email. The system we put in place should be able to create an email account for each person without much intervention if any at all(ideally).

> Redundancy

Backup mail servers to insure against partial loss.
All this really entails is making sure that emails can be verified, to check for errors or generally making sure the emails that users are looking for are available to view.

> POP/IMAP

Most servers accept IMAP and not POP anymore so our system should be setup to deal with using IMAP as it makes our email service more compatible with other services.

> Encryption

For the most part we use SSL on the mail server to retain privacy among the users.
Additionally we could setup a system to keep the emails themselves encrypted so even if someone leaked the emails all they would get back is encrypted data.

