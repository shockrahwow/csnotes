# Web Services

## Dispatcher 

Imagine then we have 1 server acting as an ingest for requests which a _dispatcher_ running on the server 


## Scaling

> Load Balancer

Primary ingest for requests which spreads the number of requests evenly across multiple.
It lets us hook more machines onto the balancer to deal with more requests over time; this is what we call _scaling out/horizontally_.

> SSL

It's especially true if you have _any_ user input that you _should absolutely use SSL_ because it means the traffic will be tunneled safely.

> Ensure that both front/back end services are prepared to **sanitize all inputs**.
