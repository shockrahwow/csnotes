# Everyone else

To some degree these notes are personal so there are a few mistakes that I just can't be bothered dealing with.
Some mistakes however are just because I wrote those notes during class, likely rushed but meh just take it with a grain of salt sometimes.

# Prelim to Students

Some of these courses are very bs'd or missing information.
This is (mostly) intentional: some courses cover lower division material that I couldn't be bothered to write down in this repository.

Also I'm basing lower division material on the curriculum of [Irvine Valley College](http://ivc.edu/Pages/default.aspx).

# Directory Guide

## 311/ - Introduction to Networking and internet Programming

## 312/ - Network Security 

## 334/ - Operating Systems

## 337/ - Introduction to Computer Architecture 

## 338/ - Software Design

**Oh boy**: I'm gonna level with you I took nearly no notes so instead I'm providing a decent source for learning design in software:


## 363/ - Introduction to Database systems

## 370/ - Algorithms

## 412/ - Network Administration
