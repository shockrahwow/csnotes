California State University Monterey Bay

SBS 385 *Environmental History of California*

Excerpts from *California: A History* (Starr 2005) CH 5 Regulation,
Railroad, and Revolution: Achievement and Turmoil in the New State

Who owned the state, anyway? Very few of the original land grantees had
come through the process with their holdings, it was a significant
**transfer of wealth**. The question of **land grants** – their origins
in the Hispanic era, their validity or invalidity, the lives that were
made or ruined by titles confirmed or denied – emerged as one of the
important themes of 19^th^ century California, a situation that would be
compounded when the railroad became the largest landowner. California
would remain resistant to small farming. Extensive holdings, together
with the quasi-feudal economy they encourage, continued to dictate the
structure of agriculture.

Landowners of Southern California argued for a separate territory south
of San Louis Obispo while statehood was being debated in Washington DC
as an effort to extend slavery to the Pacific Coast. The real impetus
behind dividing California came from the fact that the state was truly
two, and perhaps even four, **distinct places**: the urbanizing San
Francisco Bay Area and the mining districts; the Far North; the Central
Valley; and a sparsely settled Southern California, significantly
Mexican. The question of dividing the state, while it has grown
increasingly impractical over the years, has never fully gone away.

It was all happening so quickly! Not for California would there be- nor
would there ever be, as it turned out- a deliberate process of
**development**. Mining, first gold then silver, paced the foundation
and first growth of American California. Agriculture was destined to
dominate the next sequence of development. The hinterlands of San
Francisco from San Jose to Healdsburg were developing into productive
farms, Marin had a thriving dairy industry, and in 1859 people were
extolling the future of vinticulture and wine-making. The **Gold Rush**
gave a second wind to the cattle industry.

A number of Hispanic males, displaced by the new order, took to the
hills as bandits (or freedom fighters a later generation would write).
Prominent **Californios** maintained their positions, but theirs was,
ultimately, a time of gradual decline: a twilight of splendor that, even
as it waned, would in the 1880s and 1890s be reappropriated by a
generation of **Anglo** Southern Californians anxious to graft
themselves onto the rootstock of a romantic past. As the Civil War
approached, **Hispanic** sentiment was overwhelmingly **Unionist**, and
political sentiment in the state was overwhelmingly pro-Union.

It was national policy to extend the **railroad** across the continent.
The best and winning argument for the project was the outbreak of the
**Civil War** in April 1861. The Union Pacific was to build westward
from Omaha and the Central Pacific to build eastward from Sacramento. In
return, these two companies would be subsidized by an extensive federal
package of loans, bonds, cash payments, and land grants. In 1864, the
**incentive package** was increased to allocation of land grants on
alternate sections, forty miles in length, of the entire line which
would eventually make the Central Pacific and the Union Pacific, thanks
to the federal government, two of the largest landowners in the Far
West.

Four Sacramento businessmen, Huntington and Hopkins (hardware), Stanford
(groceries), and Crocker (dry goods) were seemingly ordinary men, **the
Big Four** as history would know them. Stanford, serving simultaneously
as governor and president of the Central Pacific, broke ground on what
turned out to be a six year epic of construction. Huntington would take
care of lobbying in Washington, Stanford would see to state government,
Crocker would supervise construction, and Hopkins would keep the books.

There were not enough men in California willing to do this sort of
backbreaking work at the price Crocker was willing to pay. Chinese were
already marginalized out of mainstream employment and eventually ten
thousand were employed. They more than proved their mettle against the
competing Irish workers of the Union Pacific. The lines connected on May
10, 1869 at Promontory Summit, Utah. What kind of world would this
postrailroad era be for California, now that it was less than a week’s
journey from the East Coast? California was now joining the national
economy, including industrial culture. What would the hundreds of
thousands of immigrants find? A better life? Or the same dreary,
grinding poverty that had motivated their immigration in the first
place?

The decade of the 1870s was not a good time for the nation or for
Europe, the worst depression thus far in American history. The president
sent federal troops into a number of cities to contain a series of
strikes against railroad companies. Marxism was making the transition
from theory to practice beginning with the foundation in London in 1864
of the International Workingmen’s Association. The violence of the
frontier era had not been banished by the railroad. Far from it:
violence seethed beneath the surface, and Chinese became the scapegoats
for collapsed expectations.

To understand the growth, one must look at San Francisco from two
perspectives. It was the dominant urban concentration on the Pacific
Coast, and was also a maritime colony of the eastern US and Europe. As
such, it replicated the economic, social, and cultural institutions of
advanced urbanism. The city began to fill with unemployed and restless
men. Irish born Kearney told crowds that the capitalists of the city
were running them into the ground and the Chinese were taking their
jobs. Was this street theater or real revolution or both?

The revised state constitution of 1879 dropped Spanish as the second
legal language and a strong anti-Chinese immigration statement was
adopted. Other provisions included regulation of corporations and
establishment of a state Railroad Commission. It was considered a
failure in its refusal to take up the issue of land monopoly. California
was, for all practical purposes, empty. Where were the flourishing
cities and townships, the family farms, the signs of human progress and
civility? Instead, one saw endless steppes, an occasional shack housing
alien labor. Why was this so? Because so very few people owned most of
the land in the state. After all the fuss and bother, the riots and
sandlot rallies, the incendiary rhetoric and pickax brigades, the
marches and countermarches of state militia and demonstrators, the
fistfights and gunfire, the four dead bodies, and the overhauling of the
state constitution, California entered the 1880s as a state in which
railroads, corporations, and large landowners continued to call the
shots.
