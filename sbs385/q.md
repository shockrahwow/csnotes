The Midterm Exam tested your ability to demonstrate an understanding of
course content.  The exam questions asked you to recall facts,
locations, and concepts that provide a foundation to the study of
human-environment relations relevant to an *environmental* history of
California.  Most of the questions asked "what?", "where?", and "when?",
so effective responses generally identified and described information.

The Final Take Home Exam is open books/notes/resources. The questions
are designed to assess your ability to analyze and apply what you are
learning in the course rather than on your skill to memorize
information. The focus is on asking you to describe and explain the
“how?” and “why?” behind historical events and to make connections
between natural processes, biological and cultural diversity; people,
their migration patterns, resource use practices, and cultural
landscapes; and local, regional, and global events in their social,
political, economic, and environmental context.

The exam format allows students to prepare a complete, accurate,
concise, and detailed single document that includes text, maps,
graphics, images, the use of citations, and a list of references similar
to a research paper.

Sources of information include the required and suggested readings and
materials posted to iLearn, including journal articles, news media
articles, reports, videos, PowerPoint presentations, maps, as well as
the midterm exam, assignments, guest speakers, class discussions, and
forums.

Grading criteria: Follow the outline provided below and use section
headings to guide the reader through the document. Cite your sources in
the text and format all citations in a reference list properly formatted
in a style of your choice (APA, MLA, Chicago etc …). Check spelling and
grammar, format as a PDF document, and post to the iLearn Final Exam
folder by **Monday May 13 by 11:55pm** (no .doc, .docx, pages, .wps,
.txt, google doc, or other styles to ensure that your formatting is not
altered when converted for grading in iLearn).

Late exams will receive a five point penalty out of a total of 100
points.

Additional points will be added to the final exam score based on the
quality and quantity of extra credit responses included in your
document.

Title Page

Include an original title for the final exam, your name, date, class
information, semester, and instructor name.

I. Introduction

II\. Environmental History

How do the tensions suggested in the Introduction of *Green Versus Gold*
and the myths and realities described in The Preface: A Nation-State and
Chapter 1 Queen Calafia’s Island in the book *California: A History*
apply to an understanding of contemporary California?

III\. Environmental Degradation

How did the commercial exploitation of resources in the 19^th^ and
20^th^ centuries as described in Chapters 2, 3, 4, and 8 of *The Death
and Life of Monterey Bay*, and Chapter 4 of *Green Versus Gold* degrade
marine and terrestrial ecosystems?

IV\. Environmental Management

Explain one or more of the primary themes/issues for *each* of the
Chapters 5-8 in *Green Versus Gold* based on your readings of the
chapters and class discussions. See project description document for a
partial list of key terms and concepts.

V. Environmental Conservation

What natural and cultural resources are protected in National Parks
and/or State Parks that you investigated? How are these resources
representative of California?

a\. Extra credit: Who was John Muir and what is his legacy in terms of
environmental conservation in California, the United States, and in the
world?

VI\. Historical contexts

John Steinbeck’s newspaper articles included in the *Harvest Gypsies*
and his observations as chronicled in the film *Journey to the Sea of
Cortez* provide examples of the social, political, economic, and
environmental context of the 1930s-1940s in California and the world.
Compare and contrast this historical context with social, political,
economic, and environmental events of the 1960s-1970s when Environmental
History became an academic field of study and the first Earth Day was
celebrated in 1970 as described in the documentary *Earth Days*.

b\. Extra credit: Expanding on your response to question VI. Historical
Contexts, describe some of the social, economic, political, and
environmental context of the contemporary period in which we currently
live that help you to understand the world.

VII\. Environmental Restoration

What is implied by the book title *The Death and Life of Monterey Bay*
and Part III The Recovery? Why do the authors suggest that Monterey Bay
be considered a case study for “good environmental news”?

VIII\. Environmental Sciences and Environmental Ethics

Describe the three ethical dilemmas in the concluding chapter of *Green
Versus Gold* and explain why an interdisciplinary approach to learning
can contribute to a future of “renewal and synthesis as nature and
culture come together in new visions and appreciation for a potentially
green *and* golden state” (Merchant 1998:pxvii).

IX\. Political Action Project

Review the CSUMB Founding Vision Statement &lt;
<https://csumb.edu/about/founding-vision-statement>&gt; and the Mission
and Strategic Plan, Core Values, and Academic Goals &lt;
<https://csumb.edu/about/mission-strategic-plan>&gt;. How can class
activities contribute to achieving the aspirations identified in these
CSUMB documents? Can you suggest class activities to improve student
engagement with campus stewardship and improve education and awareness
of sustainability-related issues on campus?

c\. Extra credit: How might a K-12 teacher incorporate environmental
history in their lesson plans? In your response, consider both classroom
and outdoor activities, including field trips to protected areas and
other places that inspire you?

X. Sense of Place

Describe a sense of place for the location you currently live, your
hometown in California, or somewhere that you have spent a significant
portion of time in California. In addition to using the four spheres
(lithosphere, atmosphere, hydrosphere, biosphere) to organize your
observations and experiences of the natural environment, provide
additional context about the place and its position in the contemporary
socio-economic context of the Five Californias found in the *Portrait of
California Report*. Is this a place that is prospering or in decline as
suggested by your observations and the report?

d\. Extra credit: How has this class contributed to your understanding of
the State of California and what are any recommendations to improve the
course?

XII\. Conclusion

XIII\. References
